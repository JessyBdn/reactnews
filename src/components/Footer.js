import React, { Component } from 'react';
import '../css/Footer.css';

import { Link } from 'react-router-dom';

class Footer extends Component {
 
    render() {
        return (
            <div className="footer">
                <ul className="footerMenu">
                    <li><Link to="#">About us</Link></li>
					<li><Link to="#">Contact</Link></li>
					<li><Link to="#">Newsletter</Link></li>
                </ul>
            </div>
        )
    } // fin render

} // fin class

export default Footer;