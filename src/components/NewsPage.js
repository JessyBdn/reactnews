import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
//import InfiniteScroll from 'react-infinite-scroll-component';

// Composant
import News from './News';
import NewsNavbar from './NewsNavbar';

// CSS
import '../css/NewsPage.css';

// Service API News
import { isoFetch } from '../action/fetch';


class NewsPage extends Component {

    // Constructor = objet App, initialisation de l'objet 
	// this = objet app

	constructor ( props ) {

		super( props ) // Appelle le constructeur de la classe Component dont herite App
		this.state = // Creation des etats du composant App
		{
			techCrunch : { source: "", articles: [] },
			newYorkTimes : { source: "", articles: [] },
			hackerNews : { source: "", articles: [] },
			reddit : { source: "", articles: [] }
		}

	} // Fin constructor

	componentFetch( article, url ) {

		const response = isoFetch(url);
		
		response
			.then( // callback retour des donnees serveur
				data => {
					console.log(data)
					const updateState = {
						[article]: data
					} 
					console.log(updateState)

					this.setState( updateState ) // assigne dans dataNews la valeur de data
					// ecoute le changement d'etat
				}
			)
			.catch( reason => console.error(reason.message) )

	}

	componentWillReceiveProps() { // = maj state du composant

		const techCrunch ='https://newsapi.org/v1/articles?source=techcrunch&sortBy=top&apiKey=3ce0be24528e4a3e85d27164eef5dbb3';
		const newYorkTimes ='https://newsapi.org/v1/articles?source=the-new-york-times&sortBy=top&apiKey=3ce0be24528e4a3e85d27164eef5dbb3';
		const hackerNews = 'https://newsapi.org/v1/articles?source=hacker-news&sortBy=top&apiKey=3ce0be24528e4a3e85d27164eef5dbb3';
		const reddit = 'https://newsapi.org/v1/articles?source=reddit-r-all&sortBy=top&apiKey=3ce0be24528e4a3e85d27164eef5dbb3';

		this.componentFetch( "techCrunch", techCrunch )
		this.componentFetch( "newYorkTimes", newYorkTimes )
		this.componentFetch( "hackerNews", hackerNews )
		this.componentFetch( "reddit", reddit )

	}

	//componentWillMount() {} // = avant le render
	//componentDidMount() {} // = onload, apres le render, pour recevoir des données asynchrones
	//componentWillReceiveProps() {} // = maj state du composant

    render() {
        
		const { 
			techCrunch, 
			newYorkTimes, 
			hackerNews, 
			reddit 
		} = this.state;

		const nytSource = newYorkTimes.source.replace(/-/g," ");
		const hackerSource = hackerNews.source.replace(/-/g," ");
		const redditSource = reddit.source.split("-")[0];

		const newspapers = this.state;
		console.log(newspapers);

        return (
			<Grid fluid="container-fluid">
				<Row className="show-grid">
					<Col md={4} lg={3}>
						<News className="techCrunch" source={ techCrunch.source } articles={ techCrunch.articles }/>
					</Col>
					<Col md={4} lg={6}>
						<News className="newYorkTimes" source={ nytSource } articles={ newYorkTimes.articles }/>
					</Col>
					<Col md={4} lg={3}>
						<News className="reddit" source={ redditSource } articles={ reddit.articles }/>
						<News className="hackerNews" source={ hackerSource } articles={ hackerNews.articles }/>
					</Col>
				</Row>
				<Row className="show-grid">
					{/*<NewsNavbar/>*/}
				</Row>
			</Grid>
        )
    } // fin render

} // fin class

export default NewsPage;