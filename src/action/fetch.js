// Service version ES7
export async function isoFetch (url) { // Fonction générique récup news

    try {
        const response = await fetch (url) // "await" attend la réponse du fetch
        return await response.json() // Récupère les données
    }
    catch(err) {
        console.log(err)
    }

}