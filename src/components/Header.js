import React, { Component } from 'react';
import '../css/Header.css';

import { Link } from 'react-router-dom';

class Header extends Component {
 
	render() {
		return (
			<div className="header">
				<h1 className="headerTitle">React News</h1>
				<ul className="headerMenu">
					<li><Link to="/">Home</Link></li>
					<li><Link to="/news">News</Link></li>
					<li><Link to="/tweets">Tweets</Link></li>
				</ul>
			</div>
		)
	} // fin render

} // fin class

export default Header;