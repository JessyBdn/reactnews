// React
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

// Composants
import Header from './components/Header';
import TwitterPage from './components/TwitterPage';
import NewsPage from './components/NewsPage';
import Footer from './components/Footer';

class App extends Component {

	render() {
		// Contenu html:
		return ( 
			<div className="App">
				<Header/>
					<Route path="/" exact component={NewsPage}></Route>
					<Route path="/news" component={NewsPage}/>
					<Route path="/tweets" component={TwitterPage}/>
					<Redirect from="*" to="/"/>
				<Footer/>
			</div>
		);
	} // fin render

} // fin class

export default App;