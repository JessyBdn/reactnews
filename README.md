This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
It's look like this:

```
ReactNews/

  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    App.css
    App.js
    App.test.js
    index.css
    index.js
    logo.svg
    action/
      fetch.js
    components/
      Footer.js
      Header.js
      News.js
      NewsNavbar.js
      NewsPage.js
      TwitterPage.js
    css/
      App.css
      Footer.css
      Header.css
      News.css4
      NewsNavbar.css
      NewsPage.css
      TwitterPage.css
```
Get your API Key here: https://newsapi.org

### `npm install`


```
package.json
{
  "name": "news-react",
  "version": "0.1.1",
  "private": true,
  "dependencies": {
    "es6-promise": "^4.1.1",
    "isomorphic-fetch": "^2.2.1",
    "react": "^15.6.1",
    "react-bootstrap": "^0.31.0",
    "react-dom": "^15.6.1",
    "react-infinite-scroll-component": "^2.4.0",
    "react-router-dom": "^4.1.1"
  },
  "devDependencies": {
    "react-scripts": "0.9.5"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  }
}
```

Run the App : 

### `npm start`

Open [http://localhost:3000] to view it in the browser.