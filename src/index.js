// React (injecte <app> dans <div id="root"></div>)
import React from 'react';
import ReactDOM from 'react-dom';

// Router
import { BrowserRouter as Router } from 'react-router-dom';

// App
import App from './App';
import './index.css';

window.Router = Router;

ReactDOM.render(
	<Router>
    	<App/>
  	</Router>, 
  	document.getElementById('root')
);