// contenu de ma balise <ComposantTest>

import React, { Component } from 'react';
import '../css/News.css'; // CSS de mon composant 

class News extends Component {
 
 render() {
    const { source, articles } = this.props
    console.log( source )

    return (
	<div className="news">
		<ul className={ source }>
		<h2 className="newspaper">{ source }</h2>
		{
			articles.map( 
			( article, i ) =>
			<li className="article" key={ i }>
				<a href={article.url}>
					<h3 className="title">{ article.title }</h3>
					<h4 className="author">{ article.author }</h4>
					<img className="image" src={ article.urlToImage } alt=""/>
					<p className="description">{ article.description }</p>
				</a>
			</li>
			)
		}
		</ul>
	</div>
    )

  } // fin render

} // fin class

export default News;
