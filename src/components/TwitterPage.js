import React, { Component } from 'react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
//import InfiniteScroll from 'react-infinite-scroll-component';

// Composant
import Tweets from './Tweets';

// CSS
import '../css/TwitterPage.css';

// Service API Twitter
//import { isoFetch } from '../action/fetch';


class TwitterPage extends Component {

	render() {
		return (
			<Grid fluid="container-fluid">
				<Row className="show-grid">
					<Col xs={12} sm={4} lg={3}>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Deserunt atque consequuntur consequatur, repellendus dolores 
						alias expedita tenetur blanditiis nulla, saepe reprehenderit 
						qui. Labore dolorum nostrum ratione, quisquam et voluptatum 
						facilis?
						</p>
					</Col>
					<Col xs={12} sm={4} lg={6}>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Deserunt atque consequuntur consequatur, repellendus dolores 
						alias expedita tenetur blanditiis nulla, saepe reprehenderit 
						qui. Labore dolorum nostrum ratione, quisquam et voluptatum 
						facilis?
						</p>					
					</Col>
					<Col xs={12} sm={4} lg={3}>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						Deserunt atque consequuntur consequatur, repellendus dolores 
						alias expedita tenetur blanditiis nulla, saepe reprehenderit 
						qui. Labore dolorum nostrum ratione, quisquam et voluptatum 
						facilis?
						</p>
					</Col>
				</Row>
			</Grid>
		)
	} // fin render

} // fin class

export default TwitterPage;