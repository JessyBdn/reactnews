import React, { Component } from 'react';
import '../css/NewsNavbar.css';

import { Link } from 'react-router-dom';

class NewsNavbar extends Component {
 
	render() {
		return (
			<div className="newsNavbar">
				<ul className="newsMenu">
					<li><Link to="#">Liste Newspaper</Link></li>
				</ul>
			</div>
		)
	} // fin render

} // fin class

export default NewsNavbar;